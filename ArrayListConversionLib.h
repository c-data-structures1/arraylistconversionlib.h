#ifndef ARRAYLISTCONVERSIONLIB_H
#define ARRAYLISTCONVERSIONLIB_H

// * DNT
#include "Array.h/Array.h"
#include "List.h/List.h"
// * EDNT

link validItemsListFromArray(array a, bool (*valid)(item i, item args), item args, uint *numberOfItems);
array validItemsArrayFromList(link l, bool (*valid)(item i, item args), item args);
array listToArrayOfGivenSize(link HEAD, uint elementiLista);
array listToArray(link HEAD);
link arrayToList(array a);

#endif // ! ARRAYLISTCONVERSIONLIB_H
