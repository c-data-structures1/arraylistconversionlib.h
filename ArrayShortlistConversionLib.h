#ifndef ARRAYSHORTLISTCONVERSIONLIB_H
#define ARRAYSHORTLISTCONVERSIONLIB_H

// * DNT
#include "Array.h/Array.h"
#include "Shortlist.h/Shortlist.h"
typedef shortLink link;
// * EDNT

link validItemsListFromArray(array a, bool (*valid)(item i, item args), item args, uint *numberOfItems);
array validItemsArrayFromList(link l, bool (*valid)(item i, item args), item args);
array listToArrayOfGivenSize(link HEAD, uint elementiLista);
array listToArray(link HEAD);
link arrayToList(array a);

#endif // ! ARRAYSHORTLISTCONVERSIONLIB_H
